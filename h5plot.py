import os, sys, h5py, argparse

from tkinter import *
from tkinter import filedialog as fd
from tkinter import ttk

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure


def flexx( o, r = 0, c = 0, rw = 1, cw = 1 ):
    '''flexx will control grid manager static|dymanic growth'''
    if r != None:
        o.rowconfigure( r, weight = rw )
    if c != None:
        o.columnconfigure( c, weight = cw )

def grid( r = 0, c = 0, s = 'nsew', rs = 1, cs = 1 ) ->'Grid manager settings':
    '''grid( r = 0, c = 0, s = nsew, rs = 1, cs = 1 )'''
    return dict( row=r, column=c, rowspan=rs, columnspan=cs, sticky=s )

class Utils():
    def __init__(self):
        pass

    def center(self, win):
        """
        centers a tkinter window
        :param win: the main window or Toplevel window to center
        """
        win.update_idletasks()
        width = win.winfo_width()
        frm_width = win.winfo_rootx() - win.winfo_x()
        win_width = width + 2 * frm_width
        height = win.winfo_height()
        titlebar_height = win.winfo_rooty() - win.winfo_y()
        win_height = height + titlebar_height + frm_width
        x = win.winfo_screenwidth() // 2 - win_width // 2
        y = win.winfo_screenheight() // 2 - win_height // 2
        win.geometry("{}x{}+{}+{}".format(width, height, x, y))
        win.deiconify()
        self.px = x
        self.py = y

    def scrollbar(self, popup):
        #frame_main = Frame(popup)
        #frame_main.grid(sticky="n")

        # Create a frame for the canvas with non-zero row&column weights
        frame_canvas = Frame(popup)
        frame_canvas.grid(grid())
        frame_canvas.grid_rowconfigure(0, weight=1)
        frame_canvas.grid_columnconfigure(0, weight=1)
        # Set grid_propagate to False to allow resizing later
        frame_canvas.grid_propagate(False)

        # Add a canvas in that frame
        canvas = Canvas(frame_canvas)
        canvas.grid(row=0, column=0, sticky="news")

        # Link scrollbars to the canvas
        vsb = Scrollbar(frame_canvas, orient="vertical", command=canvas.yview)
        vsb.grid(row=0, column=1, sticky="ns")
        hsb = Scrollbar(frame_canvas, orient="horizontal", command=canvas.xview)
        hsb.grid(row=1, column=0, sticky="ew")
        canvas.configure(xscrollcommand=hsb.set, yscrollcommand=vsb.set)

        # Create a frame to contain the buttons
        frame_buttons = Frame(canvas)
        canvas.create_window((0, 0), window=frame_buttons, anchor="nw")

        if sys.platform=="darwin": # if MacOS, allow for mouse scrolling
            frame_buttons.bind('<Enter>', self._bound_to_mousewheel)
            frame_buttons.bind('<Leave>', self._unbound_to_mousewheel)

        self.canvas = canvas
        self.frame_canvas = frame_canvas
        self.frame_buttons = frame_buttons

    def _bound_to_mousewheel(self, event):
        self.canvas.bind_all("<MouseWheel>", self._on_mousewheel)

    def _unbound_to_mousewheel(self, event):
        self.canvas.unbind_all("<MouseWheel>")

    def _on_mousewheel(self, event):
        self.canvas.yview_scroll(int(-1*(event.delta)), "units")


class H5PLOT(Utils):
    def __init__(self, file=None):
        super().__init__()

        self.fenetre = Tk()
        self.fenetre.title("PLOT HDF5")

        self.winx = 600 # width
        self.winy = 250 # height
        self.fenetre.geometry(f"{self.winx}x{self.winy}")

        self.file_arg = file
        self.Select_file()

        self.extra_params_nb=0
        self.Select_params()

        self.center(self.fenetre)

        self.Plotting()

        self.fenetre.mainloop()

    def Select_file(self):
        self.init_frame = Frame(self.fenetre)
        self.init_frame.pack(padx=10, pady=10)

        Button(self.init_frame, text="Select file", command=lambda:[
            self._select_file(),
            self.fenetre.geometry("")
        ]).pack()

        if self.file_arg is not None:
            self.filename = self.file_arg
            self.file_label = Label(
                self.init_frame,
                text=os.path.basename(self.filename) + " file selected",
            )
            self.file_label.pack()

        ttk.Separator(self.fenetre, orient="horizontal").pack(fill="x")

    def _select_file(self):
        filetypes = (("hdf5 files", "*.hdf5 *.h5"), ("All files", "*.*"))

        self.filename = fd.askopenfilename(
            title="Select HDF5 file", initialdir="~", filetypes=filetypes
        )
        self.filename = os.path.abspath(self.filename)

        try:
            self.file_label.destroy()
        except:
            pass
        self.file_label = Label(
            self.init_frame,
            text=os.path.basename(self.filename) + " file selected",
        )
        self.file_label.pack()

    def Select_params(self):
        self.param_frame = Frame(self.fenetre)
        self.param_frame.pack(padx=10, pady=10)

        self.param_l_frame = Frame(self.param_frame)
        self.param_l_frame.pack(side=LEFT)
        self.param_r_frame = Frame(self.param_frame)
        self.param_r_frame.pack(side=LEFT)

        Button(self.param_l_frame, text="Select X axis", command=lambda:[
            self._select_param("X")]).pack(padx=50)
        Button(self.param_r_frame, text="Select Y axis", command=lambda:[
            self._select_param("Y")]).pack(padx=50)
        # on ok button check that dataset has been selected, not a group


        # add extra parameters for additional curves
        self.extra_param_frame = Frame(self.fenetre)
        self.extra_param_frame.pack(padx=10, pady=10)
        Button(self.extra_param_frame, text="Add params", command=lambda:[
            self._add_i_param(), self.center(self.fenetre)]).pack()

        # set normal or log scale
        self.xyscale_frame = Frame(self.fenetre)
        self.xyscale_frame.pack(padx=10, pady=10)
        Button(self.xyscale_frame, text="normal scale",
            command=self.draw_plot).pack(side=LEFT)
        Button(self.xyscale_frame, text="log log scale",
            command=lambda:[self.draw_plot(logscale=True)]).pack(side=LEFT)


    def _select_param(self, param_ax):
        popup = Toplevel(self.fenetre)
        popup.title("Select param")
        flexx(popup)

        # add scrollbar
        self.scrollbar(popup)

        self.param = StringVar()
        self.param.set(0)

        # get groups
        with h5py.File(self.filename, "r") as hf:
            path_to_group = []
            hf.visit(path_to_group.append)

        # create Radiobuttons
        for row_i, groupath in enumerate(path_to_group):
            group_i = groupath.split("/")[-1]
            column_i = len(groupath.split("/")) - 1
            Radiobutton(
                self.frame_buttons,
                text=group_i,
                variable=self.param,
                value=groupath,
                state="normal"
            ).grid(column=column_i, row=row_i, sticky="nw")

        # update scrolling region
        self.frame_buttons.update_idletasks()
        self.frame_canvas.config(width=600, height=600)
        self.canvas.config(scrollregion=self.canvas.bbox("all"))
        flexx(self.frame_canvas)
        flexx(self.frame_buttons)

        self.param_ax = param_ax
        self.popup = popup

        # save and exit button
        ok_button = Button(popup, text="OK", command=self.__save_and_exit)
        ok_button.grid(column=0, row=row_i+1, ipadx=20)

        # Enter key also save and exit
        popup.bind('<Return>', self.__save_and_exit)

        # focus on scrollable region
        self.frame_buttons.focus_set()
        self.center(popup)

    def _add_i_param(self):
        eframeS = "extra_param_frame"+str(self.extra_params_nb)
        setattr(self.__class__, eframeS, Frame(self.extra_param_frame))
        self.__getattribute__(eframeS).pack(padx=10)

        Button(self.__getattribute__(eframeS), text="Select X axis", command=lambda:[
            self._select_param(str(self.extra_params_nb))]).pack(padx=50, side=LEFT)
        Button(self.__getattribute__(eframeS), text="Select Y axis", command=lambda:[
            self._select_param(self.extra_params_nb+1)]).pack(padx=50, side=LEFT)
        self.extra_params_nb +=2
        self.fenetre.geometry(f"{self.winx}x{int(self.winy+self.extra_params_nb/2*28)}")

    def __save_and_exit(self, event=None):
        self._save_param(self.param, self.param_ax),
        self.popup.destroy(),
        self.draw_plot(),
        self.center(self.fenetre)

    def _save_param(self, param, param_ax):
        param_name = param.get()
        setattr(self.__class__, "param"+str(param_ax), param_name)

        if param_ax=="X":
            frame = self.param_l_frame
        elif param_ax=="Y":
            frame = self.param_r_frame
        else:
            frame = self.extra_param_frame

        # add Label on which param has been selected
        try:
            self.__getattribute__("label_param"+str(param_ax)).destroy()
        except:
            pass
        setattr(
            self.__class__,
            "label_param"+str(param_ax),
            Label(frame,text=param_name)
        )
        self.__getattribute__("label_param"+str(param_ax)).pack()

    def Plotting(self):
        plot_frame = Toplevel(self.fenetre)
        #plot_frame.pack()
        plot_frame.geometry(f"+{self.px+500}+{self.py-150}")

        fig = Figure()
        self.ax = fig.add_subplot(111)
        self.graph = FigureCanvasTkAgg(fig, master=plot_frame)
        NavigationToolbar2Tk(self.graph, plot_frame)
        self.graph.get_tk_widget().pack(side="top",fill='both',expand=True)
        self.ax.grid()

    def draw_plot(self, logscale=False):
        # if both X and Y are present then plot
        if hasattr(self.__class__, "paramX") and hasattr(self.__class__, "paramY"):
            self.ax.cla()
            self.ax.grid()
            self.ax.set_xlabel(str(self.paramX).split("/")[-1])
            self.ax.set_ylabel(str(self.paramY).split("/")[-1])

            if logscale:
                self.ax.set_xscale("log")
                self.ax.set_yscale("log")

            X, Y = self.get_data()
            self.ax.plot(X, Y, 'k')

            if self.extra_params_nb:
                paramnbX = "param"+str(self.extra_params_nb)
                paramnbY = "param"+str(self.extra_params_nb+1)
                if hasattr(self.__class__, paramnbX) and hasattr(self.__class__, paramnbY):
                    for nb in range(int(self.extra_params_nb/2)):
                        Xi, Yi = self.get_data_extra(nb*2+2)
                        self.ax.plot(Xi, Yi, label="extra"+str(nb*2))

            self.graph.draw()

    def get_data(self):
        with h5py.File(self.filename, "r") as hf:
            X = hf[self.paramX][()]
            Y = hf[self.paramY][()]
        return X, Y

    def get_data_extra(self, nb):
        with h5py.File(self.filename, "r") as hf:
            Xi = hf[self.__getattribute__("param"+str(nb))][()]
            Yi = hf[self.__getattribute__("param"+str(nb+1))][()]
        return Xi, Yi



def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', nargs="?", help='hdf5 file to visualize.')
    args = parser.parse_args()

    H5PLOT(args.filename)


if __name__ == "__main__":
    parse()
