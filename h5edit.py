# Tkinter
import tkinter as tk
from tkinter import *
from tkinter import filedialog as fd
from tkinter import ttk

# Usual libraries
import h5py, os, sys, argparse

# Personal libraries
from h5view import hierarchical
from reformat_hdf5 import Format_to_hdf5


class Utils:
    def __init__(self):
        pass

    def add_space(self, win, space=3):
        Label(win, text="", pady=space).pack()

    def add_space_grid(self, win, grid, space=3):
        Label(win, text="", pady=space).grid(row=grid[0], column=grid[1])

    def separator(self, win):
        self.add_space(win)
        sep = ttk.Separator(win, orient="horizontal")
        #sep.pack(fill="x")
        sep.pack(ipadx=200)
        self.add_space(win)

    def center(self, win):
        """
        centers a tkinter window
        :param win: the main window or Toplevel window to center
        """
        win.update_idletasks()
        width = win.winfo_width()
        frm_width = win.winfo_rootx() - win.winfo_x()
        win_width = width + 2 * frm_width
        height = win.winfo_height()
        titlebar_height = win.winfo_rooty() - win.winfo_y()
        win_height = height + titlebar_height + frm_width
        x = win.winfo_screenwidth() // 2 - win_width // 2
        y = win.winfo_screenheight() // 2 - win_height // 2
        win.geometry("{}x{}+{}+{}".format(width, height, x, y))
        win.deiconify()


    def _select_file_return(self):
        filetypes = (("hdf5 files", "*.hdf5 *.h5"), ("All files", "*.*"))

        filename = fd.askopenfilename(
            title="Select HDF5 file", initialdir="~", filetypes=filetypes
        )
        filename = os.path.abspath(filename)
        return filename



class FrameScrollbar:
    def __init__(self):
        pass

    def get_vertically_scrollable_frame(self, parent_frame: Frame or Tk, x_scroll=False) -> Frame:
        """
        :param parent_frame: The frame to place the canvas and scrollbar onto.
        :return: A scrollable Frame object nested within the parent_frame object.
        """
        assert isinstance(parent_frame, Frame) or isinstance(parent_frame, Tk)
        self.x_scroll = x_scroll

        # Create the canvas and scrollbar.
        canvas = Canvas(master=parent_frame)
        scrollbar = Scrollbar(master=parent_frame, orient=VERTICAL, command=canvas.yview)
        if x_scroll:
            scrollbar2 = Scrollbar(master=parent_frame, orient=HORIZONTAL, command=canvas.xview)

        # Let the canvas and scrollbar resize to fit the parent_frame object.
        parent_frame.rowconfigure(0, weight=1)
        parent_frame.columnconfigure(0, weight=1)
        canvas.grid(row=0, column=0, sticky='news')
        scrollbar.grid(row=0, column=1, sticky='nes')
        if x_scroll:
            scrollbar2.grid(row=1, column=0, sticky='ews')

        # Link the canvas and scrollbar together.
        canvas.configure(yscrollcommand=scrollbar.set)
        if x_scroll:
            canvas.configure(xscrollcommand=scrollbar2.set)
        canvas.bind('<Configure>', lambda x: canvas.configure(scrollregion=canvas.bbox("all")))

        # Create the Frame that is within the canvas.
        canvas_frame = Frame(master=canvas)
        canvas.create_window((0, 0), window=canvas_frame, anchor="nw")

        canvas.columnconfigure(0, weight=1)
        canvas.rowconfigure(0, weight=1)

        canvas.create_window((0, 0), window=canvas_frame, anchor="nw", tags=("canvas_frame",))
        canvas.bind('<Configure>', self.handle_resize)

        if sys.platform=="darwin": # if MacOS, allow for mouse scrolling
            self.canvas = canvas
            canvas.bind('<Enter>', self._bound_to_mousewheel)
            canvas.bind('<Leave>', self._unbound_to_mousewheel)

        return canvas_frame

    def handle_resize(self, event):
        canvas = event.widget
        canvas_frame = canvas.nametowidget(canvas.itemcget("canvas_frame", "window"))
        min_width = canvas_frame.winfo_reqwidth()
        min_height = canvas_frame.winfo_reqheight()
        if min_width < event.width:
            canvas.itemconfigure("canvas_frame", width=event.width)
        if min_height < event.height:
            canvas.itemconfigure("canvas_frame", height=event.height)

        canvas.configure(scrollregion=canvas.bbox("all"))

    def _bound_to_mousewheel(self, event):
        self.canvas.bind_all("<MouseWheel>", self._on_mousewheel)
        if self.x_scroll:
            self.canvas.bind_all("<Shift-MouseWheel>", self._on_mousewheel2)

    def _unbound_to_mousewheel(self, event):
        self.canvas.unbind_all("<MouseWheel>")

    def _on_mousewheel(self, event):
        self.canvas.yview_scroll(int(-1*(event.delta)), "units")

    def _on_mousewheel2(self, event):
        self.canvas.xview_scroll(int(-1*(event.delta)), "units")


class Edit(Utils, FrameScrollbar):
    def __init__(self):
        super().__init__()

    def edit_test(self, win, ID=0):
        self._h5_radiobuttons(win, ID)
        self._h5__radiobuttons_options(ID)

    def _h5_radiobuttons(self, win, ID, otherfile=None):
        popup = Toplevel(win)
        popup.title("Select Group or Dataset")
        popup.geometry(f"600x600+{ID*50}+0")
        Label(popup, text="", width=1, height=1).grid(column=0, row=0)

        # frame with buttons and scrollbar
        frame_main = Frame(popup)
        frame_main.grid(sticky="news")
        frame_main.config(highlightbackground="black", highlightthickness=2)

        # scrollable button frame
        frame_buttons = self.get_vertically_scrollable_frame(frame_main)

        # init selection result variable
        setattr(self, f"group_{ID}", StringVar())
        self.__getattribute__(f"group_{ID}").set(0)

        # Collect HDF5 names
        if otherfile is None:
            file = self.filename
        else:
            file = otherfile
        with h5py.File(file, "r") as hf:
            path_to_group = []
            hf.visit(path_to_group.append)

            attrs = {}
            for group in path_to_group:
                if len(hf[group].attrs.keys()):
                    attrs[group] = dict(hf[group].attrs.items())

        # Create button for each group/dataset and attrs
        for row_i, groupath in enumerate(path_to_group):
            group_i = groupath.split("/")[-1]
            column_i = len(groupath.split("/")) - 1
            Radiobutton(
                frame_buttons,
                text=group_i,
                variable=self.__getattribute__(f"group_{ID}"),
                value=groupath
            ).grid(column=column_i, row=row_i, sticky="nw")

            if groupath in attrs.keys():
                attri = attrs.pop(groupath)

                for ai, (attr, value) in enumerate(attri.items()):
                    Radiobutton(
                        frame_buttons,
                        text=f"{attr}={value}",
                        variable=self.__getattribute__(f"group_{ID}"),
                        value=(groupath, attr),
                        fg='green'
                    ).grid(column=column_i+1+ai, row=row_i, sticky="nw")

        popup.update_idletasks()
        setattr(self, f"popup{ID}", popup)

    def _h5__radiobuttons_options(self, ID):
        popup = self.__getattribute__(f"popup{ID}")
        frame_second = Frame(popup)  # frame for options below h5view
        frame_second.grid(sticky="news")

        popup.columnconfigure(0, weight=1)
        popup.rowconfigure(1, weight=1)

        frame_second.rowconfigure(0, weight=1)
        frame_second.columnconfigure(0, weight=1)

        self.add_space_grid(frame_second, (0, 0))

        Button(
            frame_second,
            text="Add content to group",
            command=lambda: [self._add_C_to_group(popup, ID)],
        ).grid(row=1, column=0)

        Button(
            frame_second,
            text="Rename group",
            command=lambda: [
                self._h5_options_template(
                    popup, frame_second, ID, self.__rename_group, 1, "Save new name"
                )
            ],
        ).grid(row=1, column=1)
        Button(
            frame_second,
            text="Create group",
            command=lambda: [
                self._h5_options_template(
                    popup, frame_second, ID, self.__create_group, 2, "Save new group"
                )
            ],
        ).grid(row=1, column=2)
        Button(
            frame_second,
            text="Add/Edit attrs",
            command=lambda: [
                self._h5_options_template(
                    popup, frame_second, ID, self.__add_attrs, 3, "Save attr", two_=True
                )
            ],
        ).grid(row=1, column=3)

        self.add_space_grid(frame_second, (2, 0))

        popup.update_idletasks()
        self.width = popup.winfo_width()
        self.height = popup.winfo_height()


    def _add_C_to_group(self, popup, ID):
        res_group = self.__getattribute__(f"group_{ID}").get()
        res_group += "/"

        ID = 1 # because creating new window, new group_

        # ask for file
        file = self._select_file_return()
        # _h5_radiobuttons
        self._h5_radiobuttons(popup, ID, otherfile=file)
        # save button
        self._h5__radiobuttons_options_save(ID, res_group, file)

    def _h5__radiobuttons_options_save(self, ID, res_group, file):
        popup = self.__getattribute__(f"popup{ID}")
        frame_second = Frame(popup)  # frame for options below h5view
        frame_second.grid(sticky="news")

        popup.columnconfigure(0, weight=1)
        popup.rowconfigure(1, weight=1)

        frame_second.rowconfigure(0, weight=1)
        frame_second.columnconfigure(0, weight=1)

        self.add_space_grid(frame_second, (0, 0))

        # EXIT
        Button(
            frame_second,
            text="Save and Exit",
            command=lambda: [
                self.__save_C_to_group(self, res_group, ID, file),
                popup.destroy(),
            ],
        ).grid(row=0, column=0)

    def __save_C_to_group(self, event, res_group, ID, file, *arg):
        res_C = self.__getattribute__(f"group_{ID}").get()
        fmt = Format_to_hdf5()
        if file!=self.filename:
            with h5py.File(file, "r") as origin, h5py.File(self.filename, "r+") as dest:
                if res_C==0: # if whole file
                    # load entire file
                    file_dict = fmt.load_dict_from_hdf5(file)
                else: # if Group or dataset
                    file_dict = fmt._recursiv_dict_from_group(origin, res_C)

                # save in res_group
                fmt._recursiv_dict_to_hdf5(dest, file_dict, path=res_group)
        else:
            with h5py.File(self.filename, "r+") as dest:
                if res_C==0: # if whole file
                    # load entire file
                    file_dict = fmt.load_dict_from_hdf5(self.filename)
                else: # if Group or dataset
                    file_dict = fmt._recursiv_dict_from_group(dest, res_C)

                # save in res_group
                fmt._recursiv_dict_to_hdf5(dest, file_dict, path=res_group)


    def _h5_options_template(self, popup, frame, ID, func, col, butext, two_=False):
        row_i = 2
        newname = StringVar()
        Entry(frame, textvariable=newname).grid(row=row_i, column=col)
        if two_:
            row_i += 1
            newattr_value = StringVar()
            Entry(frame, textvariable=newattr_value).grid(row=row_i, column=col)

            Button(
                frame,
                text=butext,
                command=lambda: [func(ID, newname, newattr_value)],
            ).grid(row=row_i + 1, column=col)
        else:
            Button(frame, text=butext, command=lambda: [func(ID, newname)],).grid(
                row=row_i + 1, column=col
            )

        Label(frame, text="\t", pady=3).grid(row=row_i + 2, column=0)  # spacer
        popup.geometry(f"{self.width}x{self.height+100}+0+0")

    def __rename_group(self, ID, newname, *arg):
        res_group = self.__getattribute__(f"group_{ID}").get()
        res_group_parent = "/".join(res_group.split("/")[:-1]) + "/"
        res_group += "/"

        with h5py.File(self.filename, "r+") as hf:

            if isinstance(hf[res_group], h5py._hl.group.Group):  # if group
                fmt = Format_to_hdf5()
                if len(list(hf[res_group].keys())) == 0:  # if empty group
                    del hf[res_group]
                    hf[res_group_parent].create_group(newname.get())
                else:  # if group not empty
                    temp = fmt._recursiv_dict_from_group(hf, res_group)
                    del hf[res_group]
                    hf[res_group_parent].create_group(newname.get())
                    fmt._recursiv_dict_to_hdf5(
                        hf, temp, res_group_parent + newname.get() + "/"
                    )
            elif isinstance(hf[res_group], h5py._hl.dataset.Dataset):  # if dataset
                temp = hf[res_group][()]
                del hf[res_group]
                hf[res_group_parent][newname.get()] = temp
            else:
                raise RuntimeError(
                    "issue with selected group type, ", type(hf[res_group])
                )
                sys.exit()

    def __create_group(self, ID, newgroup, *arg):
        res_group = self.__getattribute__(f"group_{ID}").get()
        with h5py.File(self.filename, "r+") as hf:
            if res_group == "0":
                hf.create_group(newgroup.get())
            else:
                hf[res_group].create_group(newgroup.get())

    def __add_attrs(self, ID, newattr_key, newattr_value):
        res_group = self.__getattribute__(f"group_{ID}").get()
        attr = None
        if ' ' in res_group: # if an attr has been selected instead of a group
            res_group, attr = res_group.split(' ')

        with h5py.File(self.filename, "r+") as hf:
            if attr:
                del hf[res_group].attrs[attr]

            hf[res_group].attrs[newattr_key.get()] = newattr_value.get()



class TextRedirector(object):
    def __init__(self, widget, tag="stdout"):
        self.widget = widget
        self.tag = tag

    def write(self, str):
        self.widget.configure(state="normal")
        self.widget.insert("end", str, (self.tag,))
        self.widget.configure(state="disabled")


class Main(Edit):
    def __init__(self, file=None):
        super().__init__()

        # init window, geometry, frames
        self._init_window()

        # Select file
        self.Select_file(self.left_frame, file)
        self.separator(self.left_frame)

        # Edit file
        Button(
            self.left_frame,
            text="Edit file",
            command=lambda: [self.edit_test(self.left_frame)],
        ).pack(ipadx=30)
        self.add_space(self.left_frame, space=100)


        self.center(self.fenetre)
        self.fenetre.mainloop()

    def _init_window(self):
        self.fenetre = Tk()
        self.fenetre.title("Edit HDF5")
        self.fenetre.geometry("1000x500")

        self.left_frame = Frame(self.fenetre)
        self.left_frame.pack(side=tk.LEFT)

        self.right_frame = Frame(self.fenetre)
        self.right_frame.pack(side=tk.LEFT, fill=BOTH, expand=True)
        self.right_frame.config(highlightbackground="black", highlightthickness=2)

        self.fenetre.bind_all("<Return>", func=self.update_show_file)

        self.add_space(self.left_frame, space=30)
        Label(self.left_frame, text="Press <Return> key to update file visualization").pack(side=tk.BOTTOM)


    def Select_file(self, win, file):
        self.init_frame = Frame(win)
        self.init_frame.pack(padx=10, pady=10)

        Button(self.init_frame, text="Select file", command=self._select_file).pack(ipadx=30)

        if file is not None:
            self.filename = file
            self.file_label = Label(
                self.init_frame,
                text=os.path.basename(self.filename) + " file selected",
            )
            self.file_label.pack()

            self.update_show_file()

    def _select_file(self):
        filetypes = (("hdf5 files", "*.hdf5 *.h5"), ("All files", "*.*"))

        self.filename = fd.askopenfilename(
            title="Select HDF5 file", initialdir="~", filetypes=filetypes
        )
        self.filename = os.path.abspath(self.filename)

        try:
            self.file_label.destroy()
        except:
            pass
        self.file_label = Label(
            self.init_frame,
            text=os.path.basename(self.filename) + " file selected",
        )
        self.file_label.pack()

        self.update_show_file()


    def update_show_file(self, event=None):
        old_stdout = sys.stdout
        if "filename" in self.__dict__:
            self.show_file()
        sys.stdout = old_stdout

    def show_file(self):
        self.right_frame.pack_forget()
        self.right_frame.pack(side=tk.LEFT, fill=BOTH, expand=True)
        frame_h5view = self.get_vertically_scrollable_frame(self.right_frame, x_scroll=True)

        text_box = Text(frame_h5view, wrap='word', width=400)
        text_box.pack(fill=BOTH, expand=True)

        # collecting hierarchical output (normally in the stdout)
        sys.stdout = TextRedirector(text_box, "stdout")
        hierarchical(self.filename, stop=0, colored=False)


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', nargs="?", help='hdf5 file to edit.')
    args = parser.parse_args()

    Main(args.filename)


if __name__ == "__main__":
    parse()
