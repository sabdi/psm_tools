# Tools

# Important note

This repository contains an exact copy of the last release of the PSM Tools.
Original authors of this repository are part of the WP12 Office.

Current remote tag: v1.6

## Installation

Steps:

`git clone https://git.ias.u-psud.fr/plato_pdc/psm_specs/psm_tools.git`

or, if you have configured an SSH Key, use:

`git clone git@git.ias.u-psud.fr:plato_pdc/psm_specs/psm_tools.git`

`cd psm_tools`

`pip install .`

Once the installation is complete, MSAP tools can be imported directly into any python code.

Visualization and edition tools are designed to be run directly from the terminal.

## MSAP Tools

These two scripts are meant to be used by all the MSAP modules, as part of the script's standardization.

**utils_wp12.py** is used by all wp120_msapX_YY.py sub-modules to fetch inputs and write outputs in the datasets.

**reformat_hdf5.py** allow to do a variety of actions around HDF5 format as collecting data, writing data, reformatting between hdf5 and csv, txt, pydict and other formats (works in both ways, from hdf5 to dict/... and from dict/... to hdf5) and more.

## Visualization & Edition Tools for HDF5 files

**h5edit** is an editor which allows to easily add, rename, create groups or dataset, in a [Tkinter]([tkinter — Interface Python pour Tcl/Tk &#8212; Documentation Python 3.11.2](https://docs.python.org/fr/3/library/tkinter.html)) window.

**h5plot** is a plotting tool designed to help quickly visualize light curves.

**h5view** is a visualizer, showing groups, attributes and datas according to the user's request directly in the terminal.

Getting started with the first two apps is pretty straightforward as you only need to call them from anywhere in the terminal to launch them. They work with Tkinter Python package, make sure it is installed on your machine.

The last tool, h5_view, can be called as follow: `$ h5view file.hdf5` and has some extra arguments to print what you want. Check `$ h5view -h` for help on these extras.

## Screenshots

h5edit:

<img title="h5_edit" src="h5_edit_snapshot.png" alt="h5_edit" data-align="center">

h5plot:

<img title="h5_plot" src="h5_plot_snapshot.png" alt="h5_plot" data-align="center">

h5view:

<img title="h5_view" src="h5_view_snapshot.png" alt="h5_view" data-align="center">
