from setuptools import setup

setup(
    name='WP12Tools',
    version='1.6',
    author='Olivier Roth',
    py_modules=['utils_wp12', 'reformat_hdf5', 'h5view', "h5edit", "h5plot"],
    description='WP120 Tools package',
    install_requires=[
        'h5py',
        'matplotlib',
        'numpy',
        'pandas',
        'pyyaml',
        'tk', # tkinter
    ],
    entry_points={
        "console_scripts": [
            "h5edit = h5edit:parse",
            "h5plot = h5plot:parse",
            "h5view = h5view:parse",
        ]
    }
)
