# HDF5 Visualizer
# Olivier Roth

import h5py
import argparse
from numpy import array


def hierarchical(file, depth=0, info=1, stop=150, colored=True, group=None):
    """
    Visualization of an HDF5 file.

    Parameters:
    -----------
    file: str
        File to visualize.

    Optional parameters:
    -------------------
    depth: int, default=0
        Max depth in the architecture to print. 0 will not set any limit.
    info: int, default=1
        Infos on the groups
        0: no info
        1: attrs and shape of the groups
        2: attrs and content (recommended for files with only one value per group,
            like a constants file)
    stop: int, default=150
        Stop printing after `stop` lines.
        If set to 0, the whole file will be printed.
        (Low for better performance as opening more groups is a costing processus.)
    colored: bool, default=True
        Print colored output or not. Results may differ between operating system.
    group: str, default=None
        Print specific Group of the file
    """
    print("Visualization of the HDF5 file",file)
    print("-------------------------------"+"-"*len(file),"\n")

    # allow for colored string in terminal
    if colored:
        CGREEN = '\033[92m'
        CYELLOW = '\033[93m'
        CBLUE = '\033[94m'
        CPURPLE = '\033[95m'
        CEND = '\033[0m'
    else:
        CGREEN = ''
        CYELLOW = ''
        CBLUE = ''
        CPURPLE = ''
        CEND = ''


    with h5py.File(file, "r") as hf:

        # collect every group/dataset names
        path_to_group = []
        hf.visit(path_to_group.append)

        # collect only path containing 'group' parameter
        if group:
            path_to_group = list(filter(lambda x: group in x, path_to_group))

        # split the paths
        path_to_group_splitted = list(map(lambda x: x.split('/'), path_to_group))

        # get the depth of every path
        every_depth = array(list(map( lambda x: len(x), path_to_group_splitted )))

        # if depth provided, update what is to be printed accordingly
        if depth!=0:
            cond_depth = every_depth<=depth
            path_to_group = array(path_to_group)[cond_depth]
            path_to_group_splitted = array(path_to_group_splitted, dtype=object)[cond_depth]
            every_depth = every_depth[cond_depth]


        # for each path, print the adequat information, taking into account fct args
        for count, name in enumerate(path_to_group):

            if count==stop and stop!=0:
                print("...")
                print("\n--------------------")
                print("Stop printing, too many groups. (Increase `stop` parameter to see more)")
                return 0

            # collect attr
            attr = []
            for key in hf[name].attrs.keys():
                attr.append([key, str(hf[name].attrs.get(key))])

            # collect dataset info, if info!=0
            if isinstance(hf[name], h5py.Dataset):
                if info==0: # no info on dataset
                    pass
                elif info==1: # shape / type
                    if hasattr(hf[name][()], "__len__") and (not isinstance(hf[name][()], (str,bytes))): # if array / list
                        info_dat = CBLUE+"; shape=%s"%str(hf[name].shape)+CEND
                    else: # if int, float, str
                        info_dat = CPURPLE+" type "+str(type(hf[name][()]).__name__)+CEND
                elif info==2: # whole content
                    info_dat = CBLUE+"; {}".format(hf[name][()])+CEND
                else:
                    raise ValueError("`info` must be 0, 1 or 2, not {}. See documentation for more details.".format(info))
            else:
                info_dat = ""

            # print actual function result
            if len(attr)==0 or info==0:
                print(" |\t"*(every_depth[count]-1),path_to_group_splitted[count][-1],info_dat)
            else:
                print(" |\t"*(every_depth[count]-1),path_to_group_splitted[count][-1], end='') # group / dataset
                for att in attr:
                    print(CYELLOW+" [ %s : %s ] "%(att[0],att[1])+CEND, end='') # attrs
                print(info_dat) # shape / content / none


class CustomFormatter(argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter):
    pass

def parse():
    parser = argparse.ArgumentParser(formatter_class=CustomFormatter)

    parser.add_argument('filename', help='hdf5 file to visualize.')
    parser.add_argument('-d','--depth',
        help='type: int. Max depth in the architecture to print. 0 for not setting a limit.',
        nargs='?', default=0, type=int)
    parser.add_argument('-i','--info', choices=[0,1,2],
        help=("type: int. Infos on the groups. \n0: no info. \n1: attrs and shape of the groups. "+
        "\n2: attrs and content (recommended for files with only one value per group, like a constants file)"),
        nargs='?', default=1, type=int)
    parser.add_argument('-s','--stop',
        help=("type: int. Stop printing after `stop` lines. \nIf set to 0, the whole file will be printed."+
        "\n(Low for better performance as opening more groups is a costing processus.)"),
        nargs='?', default=150, type=int)
    parser.add_argument('-c','--colored', choices=[0,1],
        help=("type: int. Print colored result or not"),
        nargs='?', default=1, type=int)
    parser.add_argument('-g','--group',
        help=("type: str. Group to print"),
        nargs='?', default=None)

    args = parser.parse_args()

    hierarchical(
        args.filename,
        depth = args.depth,
        info = args.info,
        stop = args.stop,
        colored = args.colored,
        group = args.group,
    )


if __name__ == "__main__":
    parse()
