import h5py, os, sys, argparse
from yaml import safe_load
from time import time
from datetime import datetime
from reformat_hdf5 import Format_to_hdf5


class Main_base:
    """
    Base to any wp120 sub-module script class.
    Contains methods to read and write I/O in HDF5 files.

    Methods:
    --------
    get_inputs()
        Collect inputs for the main function in a Python dict structure.

    write_outputs()
        Write sub-module results in given HDF5 file.

    timeit(fct)
        Wrapper for msap functions, to collect execution time.
    """
    def __init__(self):
        self.inputs = {}
        self.results = {}

        self.fmt = Format_to_hdf5()

    def timeit(self, fct, *arg, **kw) -> None:
        """
        Wrapper for msap functions, to collect execution time.
        """
        self._check_starid()

        ts = time()
        fct(*arg, **kw)
        te = time()
        self.exec_time = te - ts
        print("\nExec_time (s) ", "%.4f" % self.exec_time)

        try: # add execution time to HDF5
            with h5py.File(self.datafile, "r+") as h5f:
                if self.io_mode == 1:
                    h5f[
                        f"{self.starid}/msap{self.module_str}"+
                        "/info/exec_time_sequence"
                    ][()] += str.encode(str(self.exec_time) + " - ")
                else:
                    h5f[self.write_path].attrs.create(
                        "exec_time (s)", "%.4f" % self.exec_time)
        except:
            pass

    def _check_starid(self):
        """
        Check if given test_id is an int (used since the beginning in databases)
        or a star (new, and future way to do).

        Also deal with write_path here.
        """
        # predifined output path & io_mode determination
        with h5py.File(self.datafile, "r") as h5f:

            if any(key.startswith('test') for key in h5f.keys()) and not str(self.test_id).startswith('test'):
                test_id_prefix = "test"
            else:
                test_id_prefix = ""
            self.starid = test_id_prefix + str(self.test_id)

            if self.module==5:
                self.module_str = "5_" + str(self.submodule)[0]
            else:
                self.module_str = self.module

            if f"{self.starid}/msap{self.module_str}" in h5f: # if database is on full_msap type
                self.io_mode = 1
                self.write_path = f"{self.starid}/msap{self.module_str}/outputs"
            else:
                self.io_mode = 0
                self.write_path = f"{self.starid}/outputs_test"

        # giving priority on user output path if given
        if self.user_write_path != "":
            self.write_path = self.user_write_path

    #----------

    def get_inputs(self) -> None:
        """
        Collect adequat inputs for the main function.
        """
        assert os.path.splitext (self.datafile)[1] in ['.h5','.hdf5','.H5','.HDF5'], (
            "Issue with the input file, must be hdf5 format, not " + self.datafile,
        )
        # load msap yaml
        self._yaml_load()

        # getting inputs
        with h5py.File(self.datafile, "r") as h5f:

            if self.starid not in h5f.keys():
                raise ValueError(f"Select valid test_id, '{self.starid}' not in {h5f.keys()}.")

            # all paths in hdf5
            self._walk = list(self._traverse_dict(h5f))

            for mand_opt, value in self.msapXY_in.items():
                if value is None:
                    continue  # don't collect optional inputs if optional is empty
                for source, DPinsource in value.items():

                    for DP in DPinsource:

                        if DP.startswith("IDP_PFU_"): # if PDP/IDP issue
                            self._deal_with_pdp(h5f, mand_opt, DP)

                        else: # if no PDP/IDP issue
                            try: # try collecting DP
                                self._collect_one_param(h5f, DP)
                            except:
                                print(f"Missing (or misspelled) input : '{DP}'")
                                if mand_opt == "mandatory":
                                    print(f"KeyError: Unable to open object ({DP}) "+
                                        f"in '{self.datafile}' for test '{self.starid}'",
                                        file=sys.stderr)
                                    sys.exit()
                                elif mand_opt == "optional":
                                    print("Skipped as optional parameter.\n")

            # collecting test attrs
            self.attrs = {}
            for key in h5f[self.starid].attrs.keys():
                self.attrs[key] = h5f[self.starid].attrs.get(key)

    def _traverse_dict(self, dic, parent_key=""):
        """
        Return a dict os.walk list like.
        """
        for key, value in dic.items():
            if isinstance(value, h5py._hl.group.Group):
                yield f"{parent_key}/{key}"
                yield from self._traverse_dict(value, f"{parent_key}/{key}")
            else:
                yield f"{parent_key}/{key}"

    def _collect_one_param(self, h5f, DP, pdp_idp=None):
        path = list(filter(lambda x: x.startswith("/"+self.starid) and x.endswith(DP), self._walk))[0]
        self.inputs[DP] = self.fmt._recursiv_dict_from_group(h5f, path)

        if pdp_idp: # duplicate info on both keys if pdp_idp
            self.inputs[pdp_idp] = self.inputs[DP]

    def _deal_with_pdp(self, h5f, mand_opt, DP):
        """
        Try to collect PDP then IDP, if none is collected raise error.
        If IDP is collected then PDP will not be used.
        """
        statusPDP=0
        statusIDP=0
        try: # try collect PDP
            self._collect_one_param(h5f, DP)
            print(f"Successfully collected '{DP}'")
        except:
            statusPDP=-1
            print(f"Missing (or misspelled) input : '{DP}'")
        try: # try collect IDP
            DPrepl = DP.replace("IDP_PFU", "IDP_SAS")
            self._collect_one_param(h5f, DPrepl, DP)
            print(f"Successfully collected '{DPrepl}'")
        except:
            statusIDP=-1
            print(f"Missing (or misspelled) input : '{DPrepl}'")

        if statusPDP and statusIDP: # if none is collected
            if mand_opt == "mandatory":
                print(f"KeyError: Unable to open object ({DP}) "+
                    f"in '{self.datafile}' for test '{self.starid}'",
                    file=sys.stderr)
                sys.exit()
            elif mand_opt == "optional":
                print("Skipped as optional parameter.\n")

    def _yaml_load(self):
        with open(self.yaml_file, "r") as stream:
            msap_io = safe_load(stream)
        self.msapXY_io = msap_io
        self.msapXY_in = msap_io["submodules"][str(self.submodule).zfill(2)]["inputs"]

    #----------

    def write_outputs(self) -> None:
        """
        Write sub-module results in given HDF5 file.
        """
        with h5py.File(self.datafile, "r+") as h5f:
            outh5 = os.path.normpath(self.write_path
            ) # remove potential "/" at the end, source of errors

            for key in self.results.keys():
                if outh5 + "/" + key in h5f:
                    del h5f[outh5][key]  # delete old output keys if exist

            print("\nWriting result in %s , at %s" % (self.datafile, outh5))

            # writing results
            try:
                self.fmt._recursiv_dict_to_hdf5(h5f, self.results, path=outh5)
            except Exception as ex:
                print(ex)

            if self.io_mode==1: # if database is on full_msap type
                X_YY = "_".join([str(self.module), str(self.submodule), self.fname])
                # add source of each param
                for key in self.results.keys():
                    h5f[outh5][key].attrs.create("come_from", X_YY)

            # to match yaml file :
            msapXY_out = self.msapXY_io["submodules"][str(self.submodule).zfill(2)][
                "outputs"
            ]
            for key in msapXY_out.keys():
                if key not in self.results.keys():
                    print(f"Warning: key {key} not present in results of {self.fname}.")

            # add modification date attrs
            now = datetime.now()
            modif = str(now).split(".")[0]
            h5f[outh5].attrs.create("modified", modif)


def Parser(MsapXY_class):
    class CustomFormatter(
        argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter
    ):
        pass

    parser = argparse.ArgumentParser(formatter_class=CustomFormatter)
    parser.add_argument("-f", "--file", help="hdf5 file", required=True)
    parser.add_argument(
        "-t",
        "--test_id",
        help="Test_id in the hdf5 file",
        nargs="?",
        default=0,
    )
    parser.add_argument(
        "-w",
        "--write_path",
        help="Where to store results in the HDF5 file.",
        nargs="?",
        default="",
    )
    parser.add_argument(
        "-e",
        "--extra",
        help="Provide an extra string input, check with respective msap for more info",
        nargs="?",
        default=None,
    )

    args = parser.parse_args()
    MsapXY_class(
        args.file,
        test_id=args.test_id,
        write_path=args.write_path,
        extra=args.extra,
    )
