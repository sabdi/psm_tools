import numpy as np
import pickle, h5py, os
import pandas as pd


class Format_to_hdf5:
    """
    Format csv, pydict, dat and pow files in HDF5.
    Can also format whole folders with selected format (among the one supported
    listed above), with the method "go_through_directory".

    Methods:
    -------
    save_dict_to_hdf5(dict_p, newfilename)
        Convert dict or pydict file into HDF5 and save it.
    txt_to_hdf5(file_p, newfilename)
        Convert nd.array or any txt type file into HDF5 and save it.
    csv_to_hdf5(file_p, newfilename)
        Convert pd.DataFrame or csv file into HDF5 and save it.
    go_through_directory(path, folder, new_folder)
        Convert all pydict, pow, dat, and csv files in a specified folder into HDF5 files.
        The files are stored into "new_folder" folder in the "path" directory,
        with the same architecture as the original folder.
    load_dict_from_hdf5(filename)
        Return a dict from the HFD5 input file.
    """

    def __init__(self):
        None

    def save_dict_to_hdf5(self, dict_p, newfilename):
        """
        Convert dict or pydict file into hdf5 and save it.

        Parameters:
        ----------
        dict_p: str
            Dict or pydict filepath.
        newfilename: str
            Name for the file to be saved.
        """
        if isinstance(dict_p, dict):
            dict_s = dict_p
        if isinstance(dict_p, str):
            dict_s = self._load_dict(dict_p)

        with h5py.File(newfilename, "w") as h5file:
            self._recursiv_dict_to_hdf5(h5file, dict_s)

    def _load_dict(self, filepath):
        with open(filepath, "rb") as handle:
            data = handle.read()
        handle.close()
        dict = pickle.loads(data)
        return dict

    def _recursiv_dict_to_hdf5(self, h5file, dic, path="/"):
        """
        Save python dict in HDF5 file, at specific location in file.

        Parameters:
        -----------
        h5file: h5py._hl.files.File
            Open hdf5 file.
        dic: dict
            Python dict to save
        path: str, defaut="/"
            Where to store dict, default is root.
        """
        # argument type checking
        if not isinstance(dic, dict):
            raise ValueError("must provide a dictionary")
        if not isinstance(path, str):
            raise ValueError("path must be a string")
        if not isinstance(h5file, h5py._hl.files.File):
            raise ValueError("must be an open h5py file")

        if not path.endswith("/"):
            path += "/"

        # save items to the hdf5 file
        for key, value in dic.items():

            if isinstance(value, dict) and len(value):
                # non-empty group: recurse
                self._recursiv_dict_to_hdf5(h5file, value, path + key)

            elif value is None or (isinstance(value, dict) and not len(value)):
                # Create empty group for None or {} value
                h5file.create_group(path + key)

            else:
                value_prep = self._prepare_hdf5_dataset(value)
                # can't apply filters on scalars (datasets with shape == () )
                h5file.create_dataset(path + key, data=value_prep)


    def _prepare_hdf5_dataset(self, value):
        """Cast a python object into a numpy array in a HDF5 friendly format.

        value: Input dataset in a type that can be digested by numpy.array()
            (ex: str, list, numpy.ndarray...)
        """
        # simple strings
        if isinstance(value, str):
            value = np.string_(value)

        # Ensure our data is a np.ndarray
        if not isinstance(value, (np.ndarray, np.string_)):
            array = np.array(value)
        else:
            array = value

        # handle list of strings or np array of strings
        if not isinstance(array, np.string_):
            data_kind = array.dtype.kind
            # unicode: convert to byte strings
            # (http://docs.h5py.org/en/latest/strings.html)
            if data_kind.lower() in ["s", "u"]:
                array = np.asarray(array, dtype=np.string_)

        return array

    # _________________________________

    def load_dict_from_hdf5(self, filename):
        with h5py.File(filename, "r") as h5file:
            return self._recursiv_dict_from_group(h5file, "/")

    def _recursiv_dict_from_group(self, h5file, path):

        path = os.path.normpath(path) + "/"

        if isinstance(h5file[path], h5py._hl.dataset.Dataset):
            return h5file[path][()]

        ans = {}
        for key, item in h5file[path].items():
            if isinstance(item, h5py._hl.dataset.Dataset):
                ans[key] = item[()]
            elif isinstance(item, h5py._hl.group.Group):
                ans[key] = self._recursiv_dict_from_group(h5file, path + key + "/")
        return ans

    # _________________________________

    def txt_to_hdf5(self, file_p, newfilename="", content="perso"):
        """
        Convert nd.array or txt file into hdf5 and save it in the current directory.

        Parameters:
        ----------
        file_p: {file, str}
            File or path of the file.
        newfilename: str
            Name for the file to be saved.
            Same as the original if left empty, with the new hdf5 extension.
        content: "perso" or tuple of str, default: "perso"
            To write predefined or personal column names according to file type.
            "perso": personally write the column names in the Terminal, to use if not in MSAP3.
            ("MSAP1_02", X) with X in ("IDP", "ADP", "LC")
                "IDP" : ("t", "mergedflag"),
                "ADP" : ("t", "flareflag", "quarter"),
                "LC" : ("t","flux")
            ("MSAP3", X) with X in ("pow, dat_kplr", "dat_sim")
                "pow" : ("Frequency [muHz]", "power [ppm^2/muHz]")
                "dat_kplr": ("Time [s]", "Relative flux [ppm]", "Flux error [ppm]")
                "dat_sim": ("Time [s]", "Flux variation [ppm]", "Flag")
        """
        type = ""
        if isinstance(file_p, np.ndarray):
            data = file_p
            type_p = "array"
        elif isinstance(file_p, str):
            type_p = "file"
            data = np.genfromtxt(file_p).T
            if newfilename == "":
                newfilename = os.path.splitext(os.path.split(file_p)[1])[0] + ".hdf5"
        else:
            raise TypeError(
                "Wrong input file_p type. Only str and ndarray supported. Here type(file_p)=="
                + str(type(file_p))
            )

        auto_cols = {
            "MSAP3": {
                "pow": ("Frequency [muHz]", "power [ppm^2/muHz]"),
                "dat_kplr": ("Time [s]", "Relative flux [ppm]", "Flux error [ppm]"),
                "dat_sim": ("Time [s]", "Flux variation [ppm]", "Flag"),
            },
            "MSAP1_02": {
                "IDP": ("t", "mergedflag"),
                "ADP": ("t", "flareflag", "quarter"),
                "LC": ("t", "flux"),
            },
        }

        with h5py.File(newfilename, "w") as h5file:
            if content == "perso":
                if type_p == "file":
                    print("\nHead of the file : ")
                    print(os.system("head -n12 " + file_p))
                    print("Name the columns:")
                nb_c = len(data)
                cols_p = []
                for i in range(nb_c):
                    cols_p.append(str(input("col %d / %d : " % (i + 1, nb_c))))
                for col, key in enumerate(cols_p):
                    h5file[key] = data[col]

            elif content[0] == "MSAP3":
                if content[1] in ("pow", "dat_kplr", "dat_sim"):
                    for col, key in enumerate(auto_cols[content[0]][content[1]]):
                        h5file[key] = data[col]
                else:
                    raise ValueError(
                        "content[1] should be in 'pow', 'dat_kplr', 'dat_sim'"
                    )
            elif content[0] == "MSAP1_02":
                if content[1] in ("IDP", "ADP", "LC"):
                    for col, key in enumerate(auto_cols[content[0]][content[1]]):
                        h5file[key] = data[col]
                else:
                    raise ValueError("content[1] should be in 'IDP', 'ADP', 'LC'")
            else:
                raise ValueError("Wrong content, check docstring for more information")

    # _________________________________

    def csv_to_hdf5(self, file_p, newfilename, content=None, **kwargs):
        """
        Convert pd.DataFrame or csv file into hdf5 and save it.

        Parameters:
        ----------
        file_p: {file, str}
            Panda.DataFrame or path of the csv file.
        newfilename: str
            Name for the file to be saved.
        """
        if isinstance(file_p, pd.DataFrame):
            data = file_p
        elif isinstance(file_p, str):
            data = pd.read_csv(file_p, **kwargs)
        else:
            raise TypeError(
                "Wrong input file_p type. Only str and ndarray supported. Here type(file_p)=="
                + str(type(file_p))
            )

        if content == "perso":
            print("Head of the file:")
            print(data.head())
            print("Name the columns:")

            for col in data.columns:
                new_col = str(input("col %s : " % (str(col))))
                data.rename(columns={col: new_col}, inplace=True)
        else:
            None

        self.save_dict_to_hdf5(data.to_dict(orient="list"), newfilename)


# _________________________________


def _nan_equal(a, b, approx=False):
    try:
        if not approx:
            np.testing.assert_equal(a, b)
        else:
            np.testing.assert_almost_equal(a, b, decimal=approx)
    except AssertionError:
        return False
    return True
